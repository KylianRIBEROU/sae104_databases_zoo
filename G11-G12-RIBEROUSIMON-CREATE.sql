drop table vivre;
drop table manger;
drop table remplacer;
drop table ZoneGeo;
drop table Animal;
drop table Espece;
drop table Famille;
drop table Emplacement;
drop table stocker;
drop table Aliment;
drop table ParcZoologique; 
drop table Safari;
drop table Zoo;
drop table TypedEmplacement1;

-- les drops se font dans le sens inverse de la création des tables 


-- AUTEURS : RIBEROU Kylian 11A et SIMON Gaël 12A

create table Zoo(
    IdZoo Number(10) not null, 
    VilleZoo Varchar2(35),
    Pays Varchar2(20),
    Telephone Number(10),
    Email Varchar2(35),
    NomResponsable Varchar2(40),
    constraint ClefZoo Primary Key (IdZoo)
);

create table Safari( 
    IdZoo number(10),
    Superficie Number(10),
    modeLocomotion Varchar2(20),
    constraint clefsafari Primary key (IdZoo),
    constraint clefE_Safari Foreign Key (IdZoo) References Zoo(IdZoo)
);

create table ParcZoologique(
    nomEquipeDeRecherche Varchar2(50),
    IdZoo number(10),
    constraint clefParcZoologique Primary key (IdZoo),
    constraint clefE_ParcZoologique Foreign Key (IdZoo) References Zoo(IdZoo)
);

create table Aliment(
    IdAliment number(10),
    nomAliment varchar2(20),
    constraint ClefALiment Primary Key (IdAliment)
);

create table stocker(
    IdZoo number(10) not null,
    IdAliment number(10) not null,
    QteStockee number(10),
    constraint clefstocker1 Primary key (IdZoo,IdAliment),
    constraint clefE_stocker1 Foreign Key (IdZoo) References Zoo(IdZoo),
    constraint clefE_stocker2 Foreign Key (IdAliment) References Aliment(IdAliment)
);

create table TypedEmplacement1(
    TypeEmplacement1 Varchar2(20),
    constraint clefTypeEmplacement1 Primary Key (TypeEmplacement1)
);

create table Emplacement(
    NumEmplacement Number(10),
    Situation Varchar2(25),
    IdZoo number(10) not null,
    soustype Varchar2(20),
    TypeEmplacement1 Varchar2(20) not null,
    constraint ClefEmplacement Primary Key (NumEmplacement,IdZoo),
    constraint clefE_Emplacement1 Foreign key (IdZoo) References Zoo(IdZoo),
    constraint clefE_Emplacement2 Foreign key (TypeEmplacement1) References TypedEmplacement1(TypeEmplacement1)
);

create table Famille(
    NomFamille Varchar2(25),
    Caractéristiques Varchar2(100),
    constraint ClefFamille Primary Key (NomFamille)
);

create table Espece(
    IdEspece Number(10),
    NomScientifique Varchar2(25),
    NomVulgaire Varchar2(25),
    PopEstimee Number(15),
    NomFamille Varchar2(25) not null,
    constraint ClefEspece Primary Key (IdEspece),
    constraint clefE_espece Foreign key (NomFamille) References Famille(NomFamille)
);

create table Animal(
    IdAnimal Number(10),
    Nom Varchar2(20),
    Sexe Varchar2(10),
    Ddn Date,
    DateArrivee Date,
    Remarques Varchar2(400),
    IdEspece number(10) not null,
    NumEmplacement number(10) not null,
    IdZoo number(10) not null,
    constraint ClefAnimal Primary Key (IdAnimal),
    constraint clefE_Animal1 Foreign Key (IdEspece) References Espece(IdEspece),
    constraint clefE_Animal2 Foreign Key (NumEmplacement,IdZoo) References Emplacement(NumEmplacement,IdZoo),
    constraint clefE_Animal3 Foreign Key (IdZoo) References Zoo(IdZoo)
);

create table ZoneGeo(
    CodeZone Number(10),
    ZoneGeographique Varchar2(20),
    constraint ClefZoneGeo Primary Key (CodeZone)
);

create table remplacer(
    IdAliment_1 number(10) not null,
    IdAliment_2 number(10) not null,
    TauxSub number(10),
    constraint clefremplacer1 Primary key (IdAliment_1,IdAliment_2),
    constraint clefE_remplacer1 Foreign Key (IdAliment_1) References Aliment(IdAliment),
    constraint clefE_remplacer2 Foreign Key (IdAliment_2) References Aliment(IdAliment)
);

create table manger(
    IdAliment number(10) not null,
    IdEspece number(10) not null,
    QteMangee number(10),
    constraint clefmanger1 Primary key (IdAliment,IdEspece),
    constraint clefE_manger1 Foreign Key (IdAliment) References Aliment(IdAliment),
    constraint clefE_manger2 Foreign Key (IdEspece) References Espece(IdEspece)
);

create table vivre(
    IdEspece number(10) not null,
    CodeZone number(10) not null,
    nombre number(10),
    constraint clefvivre1 Primary key (IdEspece,CodeZone),
    constraint clefE_vivre1 Foreign key (IdEspece) References Espece(IdEspece),
    constraint clefE_vivre2 Foreign key (CodeZone) References ZoneGeo(CodeZone)
);