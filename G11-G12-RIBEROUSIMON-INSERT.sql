insert into ZOO values (12452, 'Marseille', 'France', 0645223148, 'zoodelafleche@gmail.com', 'GAETAN FIROLE');
insert into PARCZOOLOGIQUE values ('Equipe de Recherche LIFO', 12452 );

insert into ALIMENT values (1, 'Betterave');
insert into ALIMENT values (2, 'Fougeres');
insert into ALIMENT values (3, 'Bambou');
insert into ALIMENT values (4, 'Viande rouge');
insert into ALIMENT values (5, 'Graines');
insert into ALIMENT values (6, 'Insectes');
insert into ALIMENT values (7, 'Bouillie');

insert into STOCKER values (12452, 1, 1227);
insert into STOCKER values (12452, 2, 4895);
insert into STOCKER values (12452, 3, 227);
insert into STOCKER values (12452, 4, 887);
insert into STOCKER values (12452, 5, 9215);
insert into STOCKER values (12452, 6, 100);
insert into STOCKER values (12452, 7, 4219); 

insert into TYPEDEMPLACEMENT1 values ('Zone');
insert into TYPEDEMPLACEMENT1 values ('Enclos');

insert into EMPLACEMENT values (3000,'E4-B9', 12452, 'Cage','Enclos');
insert into EMPLACEMENT values (3010, 'C5-A2', 12452, 'Forêt', 'Zone');
insert into EMPLACEMENT values (3020, 'D4-E7', 12452, 'Savane', 'Zone');

insert into FAMILLE values ('Elephantides', 'Grande taille, petites oreilles, vegetarien');
insert into FAMILLE values ('Felins', 'carnivore, machoire d environ 30 dents, tête ronde au crane raccourci');
insert into FAMILLE values ('Psittaciformes', 'couleurs chatoyantes, peut imiter la voix humaine');
insert into FAMILLE values ('Reptiles', 'corps allonge et recouvert d ecailles' );

insert into ESPECE values (4010, 'Elephas', 'Elephant d Asie', 120000, 'Elephantides');
insert into ESPECE values (4011, 'Panthera Tigris', 'Tigre', 65000, 'Felins');
insert into ESPECE values (4012, 'Psittacidae', 'Perroquet', 265000,'Psittaciformes');
insert into ESPECE values (4013, 'Testudines', 'Tortue', 25000, 'Reptiles');

insert into ANIMAL values (1010, 'Charlotte', 'Femelle', to_date('22/10/2019', 'dd/mm/yyyy'), to_date('28/11/2021', 'dd/mm/yyyy'), 'Charlotte a une defense cassee. Elle est souvent stressee.', 4010, 3020, 12452);
insert into ANIMAL values (1011, 'Babar', 'Male', to_date('02/03/2017', 'dd/mm/yyyy'), to_date('11/12/2019', 'dd/mm/yyyy'), 'Babar est tres joueur. Penser a lui donner des jouets pour ne pas qu il abime l environnement.', 4010, 3020, 12452);
insert into ANIMAL values (1012, 'Tigrou', 'Male', to_date('15/09/2015', 'dd/mm/yyyy'), to_date('23/08/2021', 'dd/mm/yyyy'), 'Bien lui donner 3kilos de viande rouge a chaque repas.', 4011, 3020, 12452);
insert into ANIMAL values (1013, 'Choupine', 'Femelle', to_date('02/03/2022', 'dd/mm/yyyy'), to_date('02/03/2022', 'dd/mm/yyyy'), 'Choupine chante tres bien. Le plus joli perroquet du zoo !', 4012, 3010, 12452);
insert into ANIMAL values (1014, 'Polnareff', 'Male', to_date('29/05/1963', 'dd/mm/yyyy'), to_date('09/04/1989', 'dd/mm/yyyy'), 'En plus d avoir un joli prenom, Polnareff est la tortue la plus agee du zoo !', 4013, 3000, 12452);

insert into ZONEGEO values (5000, 'Savane');
insert into ZONEGEO values (5010, 'Desert');
insert into ZONEGEO values (5020, 'Forêt');
insert into ZONEGEO values (5030, 'Marais');
insert into ZONEGEO values (5040, 'Banquise');

insert into REMPLACER values (1, 7, 80);
insert into REMPLACER values (4, 6 , 20);
insert into REMPLACER values (2,3, 90);
insert into REMPLACER values (3,2,55);
insert into REMPLACER values (5, 7, 35);
insert into REMPLACER values (6, 7, 95);
insert into REMPLACER values (7, 5, 25);    

insert into MANGER values (1, 4013, 2);
insert into MANGER values (5, 4013, 1); 
insert into MANGER values (4, 4011, 3);
insert into MANGER values (2, 4010, 30);
insert into MANGER values (7, 4010, 50);
insert into MANGER values (3, 4012, 5);
insert into MANGER values (6, 4012, 1); 

insert into VIVRE values (4010, 5000, 2); 
insert into VIVRE values (4011, 5020, 1);
insert into VIVRE values (4012, 5020, 1);
insert into VIVRE values (4013, 5040, 1);